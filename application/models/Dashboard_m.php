<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_m extends CI_Model {

	public function selectAll($table)
	{
		$q = $this->db->get($table);
		return $q->result();
	}

	public function addRecord($table, $data)
	{
		$this->db->insert($table, $data);
	}
	
	public function updateRecord($table, $data, $id)
	{
		$this->db->update($table, $data, $id);
	}

	public function deleteRecord($table, $id)
	{
		$this->db->delete($table, $id);
	}


	function fetch_data()
	{
        $query = $this->db->get("kelas");
        return $query->result();

   }
}
