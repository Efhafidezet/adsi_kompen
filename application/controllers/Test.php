<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Test extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
    }

	public function index()
    {
        // $this->template->load('template', 'pages/form_admin'); 

        $data['the_user'] = $this->Dashboard_m->selectAll('the_user');
        $data['dosen'] = $this->Dashboard_m->selectAll('user_dosen');
        $data['mahasiswa'] = $this->Dashboard_m->selectAll('user_mahasiswa');

        $this->load->view('pages/form_test', $data);
    }

    var $temp_nama = 'xarthos69';
    var $temp_email = 'xarthos69@gmail.com';
    var $temp_token = 'asdasd';

    public function insert_db()
    {
        if(isset($_FILES["file"]["name"]))
        {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();

                for($row = 2; $row <= $highestRow; $row++)
                {   
                    $nim = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $nama = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $email = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $tempat_lahir = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $tanggal_lahir = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $jenis_kelamin = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $kodePass = md5('123');
                    $peran = 3;
                    $token = random_string('alnum', 50);

                    $tanggal_lahir = str_replace('/', "-", $tanggal_lahir);

                    $unix_date = (int)($tanggal_lahir - 25569) * 86400;
                    $tanggal_lahir =  date("Y-m-d", (int)$unix_date);

                    $tbl_u = 'the_user';
                    $tbl_m = 'user_mahasiswa';

                    //Auto Generate ID
                    $this->db->select('*');
                    $this->db->from($tbl_u);
                    $this->db->order_by('id_user', "desc"); 
                    $this->db->limit(1);

                    $res_log = $this->db->get();
                    $result_log = $res_log->result();

                    $char_id = substr($result_log[0]->id_user[0], 0, 1);
                    $id = substr($result_log[0]->id_user, 1);

                    $current_last_id = $id + 1;

                    if ($current_last_id < 10) {
                        $temp_id = 0;
                        $last_id = $char_id . '00' . $current_last_id;
                    } elseif ($current_last_id < 100) {
                        $temp_id = 0;
                        $last_id = $char_id . '0' . $current_last_id;
                    } elseif ($current_last_id < 1000) {
                        $temp_id = 0;
                        $last_id = $char_id . '' . $current_last_id;
                    }

                    $data_the_user = array(
                        'id_user'=> $last_id,
                        'email'=> $email,
                        'kodePass'=> $kodePass,
                        'peran'=> $peran,
                        'token'=> $token,
                        'aktif'=> 0
                    );

                    $data_mahasiswa = array(
                        'NIM'=> $nim,
                        'id_user'=> $last_id,
                        'id_kelas'=> 'K001',
                        'id_prodi'=> 'P001',
                        'nama'=> $nama,
                        'tempat_lahir'=> $tempat_lahir,
                        'tanggal_lahir'=> date("Y-m-d", strtotime($tanggal_lahir)),
                        'jenis_kelamin'=> $jenis_kelamin,
                    );


                    $this->Dashboard_m->addRecord($tbl_u, $data_the_user);  
                    $this->Dashboard_m->addRecord($tbl_m, $data_mahasiswa);

                    $this->temp_nama = ucwords($nama);
                    $this->temp_email = $email;
                    $this->temp_token = $token;

                    $this->sendMail();
                }
            }
        } 
    }

    function sendMail() {
        $verifurl = base_url('test/verifikasi/').$this->temp_token;
        $isi = '
                <!doctype html>
                <html>

                    <head>
                        <meta name="viewport" content="width=device-width" />
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <title>Simple Transactional Email</title>
                        <style>
                            /* -------------------------------------
                                              GLOBAL RESETS
                                          ------------------------------------- */
                            /*All the styling goes here*/
                            
                            img {
                                border: none;
                                -ms-interpolation-mode: bicubic;
                                max-width: 100%;
                            }
                            
                            body {
                                background-color: #f6f6f6;
                                font-family: sans-serif;
                                -webkit-font-smoothing: antialiased;
                                font-size: 14px;
                                line-height: 1.4;
                                margin: 0;
                                padding: 0;
                                -ms-text-size-adjust: 100%;
                                -webkit-text-size-adjust: 100%;
                            }
                            
                            table {
                                border-collapse: separate;
                                mso-table-lspace: 0pt;
                                mso-table-rspace: 0pt;
                                width: 100%;
                            }
                            
                            table td {
                                font-family: sans-serif;
                                font-size: 14px;
                                vertical-align: top;
                            }
                            /* -------------------------------------
                                              BODY & CONTAINER
                                          ------------------------------------- */
                            
                            .body {
                                background-color: #f6f6f6;
                                width: 100%;
                            }
                            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
                            
                            .container {
                                display: block;
                                margin: 0 auto !important;
                                /* makes it centered */
                                max-width: 580px;
                                padding: 10px;
                                width: 580px;
                            }
                            /* This should also be a block element, so that it will fill 100% of the .container */
                            
                            .content {
                                box-sizing: border-box;
                                display: block;
                                margin: 0 auto;
                                max-width: 580px;
                                padding: 10px;
                            }
                            /* -------------------------------------
                                              HEADER, FOOTER, MAIN
                                          ------------------------------------- */
                            
                            .main {
                                background: #ffffff;
                                border-radius: 3px;
                                width: 100%;
                            }
                            
                            .wrapper {
                                box-sizing: border-box;
                                padding: 20px;
                            }
                            
                            .content-block {
                                padding-bottom: 10px;
                                padding-top: 10px;
                            }
                            
                            .footer {
                                clear: both;
                                margin-top: 10px;
                                text-align: center;
                                width: 100%;
                            }
                            
                            .footer td,
                            .footer p,
                            .footer span,
                            .footer a {
                                color: #999999;
                                font-size: 12px;
                                text-align: center;
                            }
                            /* -------------------------------------
                                              TYPOGRAPHY
                                          ------------------------------------- */
                            
                            h1,
                            h2,
                            h3,
                            h4 {
                                color: #000000;
                                font-family: sans-serif;
                                font-weight: 400;
                                line-height: 1.4;
                                margin: 0;
                                margin-bottom: 30px;
                            }
                            
                            h1 {
                                font-size: 35px;
                                font-weight: 300;
                                text-align: center;
                                text-transform: capitalize;
                            }
                            
                            p,
                            ul,
                            ol {
                                font-family: sans-serif;
                                font-size: 14px;
                                font-weight: normal;
                                margin: 0;
                                margin-bottom: 15px;
                            }
                            
                            p li,
                            ul li,
                            ol li {
                                list-style-position: inside;
                                margin-left: 5px;
                            }
                            
                            a {
                                color: #3498db;
                                text-decoration: underline;
                            }
                            /* -------------------------------------
                                              BUTTONS
                                          ------------------------------------- */
                            
                            .btn {
                                box-sizing: border-box;
                                width: 100%;
                            }
                            
                            .btn > tbody > tr > td {
                                padding-bottom: 15px;
                            }
                            
                            .btn table {
                                width: auto;
                            }
                            
                            .btn table td {
                                background-color: #ffffff;
                                border-radius: 5px;
                                text-align: center;
                            }
                            
                            .btn a {
                                background-color: #ffffff;
                                border: solid 1px #3498db;
                                border-radius: 5px;
                                box-sizing: border-box;
                                color: #3498db;
                                cursor: pointer;
                                display: inline-block;
                                font-size: 14px;
                                font-weight: bold;
                                margin: 0;
                                padding: 12px 25px;
                                text-decoration: none;
                                text-transform: capitalize;
                            }
                            
                            .btn-primary table td {
                                background-color: #3498db;
                            }
                            
                            .btn-primary a {
                                background-color: #3498db;
                                border-color: #3498db;
                                color: #ffffff;
                            }
                            /* -------------------------------------
                                              OTHER STYLES THAT MIGHT BE USEFUL
                                          ------------------------------------- */
                            
                            .last {
                                margin-bottom: 0;
                            }
                            
                            .first {
                                margin-top: 0;
                            }
                            
                            .align-center {
                                text-align: center;
                            }
                            
                            .align-right {
                                text-align: right;
                            }
                            
                            .align-left {
                                text-align: left;
                            }
                            
                            .clear {
                                clear: both;
                            }
                            
                            .mt0 {
                                margin-top: 0;
                            }
                            
                            .mb0 {
                                margin-bottom: 0;
                            }
                            
                            .preheader {
                                color: transparent;
                                display: none;
                                height: 0;
                                max-height: 0;
                                max-width: 0;
                                opacity: 0;
                                overflow: hidden;
                                mso-hide: all;
                                visibility: hidden;
                                width: 0;
                            }
                            
                            .powered-by a {
                                text-decoration: none;
                            }
                            
                            hr {
                                border: 0;
                                border-bottom: 1px solid #f6f6f6;
                                margin: 20px 0;
                            }
                            /* -------------------------------------
                                              RESPONSIVE AND MOBILE FRIENDLY STYLES
                                          ------------------------------------- */
                            
                            @media only screen and (max-width: 620px) {
                                table[class=body] h1 {
                                    font-size: 28px !important;
                                    margin-bottom: 10px !important;
                                }
                                table[class=body] p,
                                table[class=body] ul,
                                table[class=body] ol,
                                table[class=body] td,
                                table[class=body] span,
                                table[class=body] a {
                                    font-size: 16px !important;
                                }
                                table[class=body] .wrapper,
                                table[class=body] .article {
                                    padding: 10px !important;
                                }
                                table[class=body] .content {
                                    padding: 0 !important;
                                }
                                table[class=body] .container {
                                    padding: 0 !important;
                                    width: 100% !important;
                                }
                                table[class=body] .main {
                                    border-left-width: 0 !important;
                                    border-radius: 0 !important;
                                    border-right-width: 0 !important;
                                }
                                table[class=body] .btn table {
                                    width: 100% !important;
                                }
                                table[class=body] .btn a {
                                    width: 100% !important;
                                }
                                table[class=body] .img-responsive {
                                    height: auto !important;
                                    max-width: 100% !important;
                                    width: auto !important;
                                }
                            }
                            /* -------------------------------------
                                              PRESERVE THESE STYLES IN THE HEAD
                                          ------------------------------------- */
                            
                            @media all {
                                .ExternalClass {
                                    width: 100%;
                                }
                                .ExternalClass,
                                .ExternalClass p,
                                .ExternalClass span,
                                .ExternalClass font,
                                .ExternalClass td,
                                .ExternalClass div {
                                    line-height: 100%;
                                }
                                .apple-link a {
                                    color: inherit !important;
                                    font-family: inherit !important;
                                    font-size: inherit !important;
                                    font-weight: inherit !important;
                                    line-height: inherit !important;
                                    text-decoration: none !important;
                                }
                                .btn-primary table td:hover {
                                    background-color: #34495e !important;
                                }
                                .btn-primary a:hover {
                                    background-color: #34495e !important;
                                    border-color: #34495e !important;
                                }
                            }
                        </style>
                    </head>

                    <body class="">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
                            <tr>
                                <td>&nbsp;</td>
                                <td class="container">
                                    <div class="content">

                                        <!-- START CENTERED WHITE CONTAINER -->
                                        <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
                                        <table role="presentation" class="main">

                                            <!-- START MAIN CONTENT AREA -->
                                            <tr>
                                                <td class="wrapper">
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <p>Hallo '.$this->temp_nama.'!</p>
                                                                <br>
                                                                <a href="'.$verifurl.'">
                                                                    <p>Verifikasikan akunmu disini ya!</p>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <!-- END MAIN CONTENT AREA -->
                                        </table>

                                        <!-- START FOOTER -->
                                        <div class="footer">
                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="content-block powered-by">
                                                        <a href="http://htmlemail.io">ADSI - Sistem Absensi</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!-- END FOOTER -->

                                        <!-- END CENTERED WHITE CONTAINER -->
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </body>

                    </html>';

        global $error;
        $mail = new PHPMailer();  // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 2;  // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true;  // authentication enabled
        $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
        $mail->SMTPAutoTLS = false;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isHTML(true);

        $mail->Username = 'akungacha225@gmail.com';  
        $mail->Password = 'kemalidris123';           
        $mail->SetFrom('sak_ccit5a@gmail.com', 'Sistem Absensi Kehadiran');
        $mail->Subject = 'Verifikasikan Akunmu!';
        $mail->Body = $isi;
        $mail->AddAddress($this->temp_email);

        if(!$mail->Send()) {
            $error = 'Mail error: '.$mail->ErrorInfo; 
            return false;
        } else {
            $error = 'Message sent!';
            return true;
        }
    }

    function verifikasi($token)
    {
        $tbl_u = 'the_user';

        $this->db->select('*');
        $this->db->from($tbl_u);
        $this->db->where('token', $token);
        $this->db->limit(1);

        $res_log = $this->db->get();
        $result_log = $res_log->result();

        if ($result_log == null) {
            echo "Ada kesalahan token verifikasi. Silahkan cek kembali ya!";
        } else {
            if ($result_log[0]->aktif == 0) {
                $data = array(
                    'aktif' => 1
                );
        
                $id = array(
                    'id_user' => $result_log[0]->id_user
                );

                $this->Dashboard_m->updateRecord($tbl_u, $data, $id);

                // echo "Akunmu telah berhasil diverifikasi. Silahkan login kembali ya!";
                redirect(base_url().'user/login/');
            } else {
                echo "Ada kesalahan token verifikasi. Silahkan cek kembali ya!";
            }
        }
        

        
    }
}
