<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['akun'] = $this->Dashboard_m->selectAll('the_user');
        $data['judul'] = "akun";
		$this->template->set('title', 'akun');
        $this->template->load('template', 'pages/akun', $data);
    }
}